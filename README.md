# eks-example

This example shows howto create a EKS Cluster

## Requirements
AWS Account with admin access.

## Steps

- Create a user with admin access, programatic access too (call him eks). FIXME: Review required permissions
- Create a role with permision to control eks (eks-role)
- Create  a SecurityGroup with https ingress permisions (named eks_https_in)
- Login with eks user
- Go to EKS and create a cluster (name eks_cluster)
    - Choose the VPC
    - Select all subnets on the diferents availability zones
    - Choose the previously created role "eks-role"
    - Choose the security group "eks_sg_https_in"
    - Create cluster
- Install kubectl
    - Configure kubectl to use Amazon EKS (module aws-iam-authenticator)
    ```bash
    curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
    chmod +x ./aws-iam-authenticator
    mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH
    echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
    ```

- Configure awscli
    - Install

    ```bash
    pip install awscli
    ```
    - Configure awscli with the credentials of eks user

    ```bash
    $ aws configure
    AWS Access Key ID: XXXXXXXXXXXX
    AWS Secret Access Key: YYYYYYYYYY
    Default region name [us-west-2]: us-west-1
    Default output format [None]: json
    ```
- Update kubeconfig to use aws-iam-authenticator
    - Check if eks_cluster is ready (its take 10min to be create)

    ```bash
    aws eks describe-cluster --name eks_cluster|grep status
    ```
    - Update config (its modify ~/.kube/config to use aws-iam-authenticator)

    ```bash
    aws eks update-kubeconfig --name eks_cluster
    ```
- Create a LoadBalancer

```bash
kubectl apply -f servicio.yaml
```
- Verify the creation
```bash
kubectl get all
```
